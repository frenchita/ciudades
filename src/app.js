import express from 'express'
import 'dotenv/config';
import routes from './routes';

const app = express()
var cors = require('cors')
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(routes());

app.get('/', (req, res) => {
    res.send({
        msj: 'Hola Mundo :-)'
    })
})

app.listen(process.env.PORT, ()=>{
    console.log(`Server started in port ${process.env.PORT}`);
})

