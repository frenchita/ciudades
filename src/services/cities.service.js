import _ from "underscore"
import mongoose from "../config/mongo"

const city_model = mongoose.model('cities', { id: Number, city: String, country: String });

export const enrich = async (cities) => {

    let values = cities.map(city => city.id);

    const enrich_cities = []

    return await city_model.find({
        'id' : values
    }, { _id: 0 }).lean().select('id city country')
} 