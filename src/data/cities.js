import Database, { SELECT } from "../config/mysql";

class Cities {

  static async get() {
    
    const rows = await Database.query(
        "SELECT id FROM cities ORDER BY RAND() LIMIT 5",
        {
            type: SELECT
        }
    );
    return rows;
  }

}

export default Cities;
