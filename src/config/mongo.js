import mongoose from "mongoose"


mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`, {useNewUrlParser: true, useUnifiedTopology: true});

module.exports = mongoose