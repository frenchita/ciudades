import Sequelize, { QueryTypes } from 'sequelize';

export const SELECT = QueryTypes.SELECT;
export const INSERT = QueryTypes.INSERT;
export const UPDATE = QueryTypes.UPDATE;

const Database = new Sequelize(
	
	process.env.MYSQL_DATABASE,
	process.env.MYSQL_USER,
	process.env.MYSQL_PASSWORD,
	
	{
		host: process.env.MYSQL_HOST,
		dialect: 'mysql',
		logging: function(query) {
			console.log(query);
		}
	}
);

export default Database;