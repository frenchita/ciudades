import redis from "redis"

const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
 
client.on("error", function(error) {
  console.error(error);
});

module.exports = {
    redis,
    client
};