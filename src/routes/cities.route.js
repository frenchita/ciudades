import { Router } from "express";
import Cities from "../data/cities";
import { enrich } from "../services/cities.service";
import { client, redis } from "../config/redis";
import bluebird from 'bluebird'
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

import axios from 'axios'

const route = Router();

export default app => {
    app.use("/cities", route);
    
    route.get("/", async (req, res) => {
		const cities = await Cities.get();
        const cities_enriches = await enrich(cities);

        Promise.all(
            cities_enriches.map(city => {

                var key = `city_key_${city.id}`

                return client.getAsync(key).then(async (data) => {
        
                    if(data !== null){
                        city.temp = data;
                        return city;
                    }else{
                    
                        const api_key = process.env.openweathermap_api_key
                        const endpoint = `https://api.openweathermap.org/data/2.5/weather?q=${city.city}, ${city.country}&appid=${api_key}&mode=json&units=metric`
                        
                        return axios(endpoint).then((data)=>{
                            client.set(key, data.data.main.temp)
                            client.expire(key, process.env.cache_ttl);
                            city.temp = data.data.main.temp;
                            return city;
                        })
                    }
                })
            })    
        ).then(cities => {
            res.status(200).json({cities: cities });
        }).catch(err =>{
            res.status(400).json({msj: 'error data' });
            console.error(res)
        })



    	
    });

};