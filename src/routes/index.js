import { Router } from 'express';

 
// import routes and add to export
import cities from './cities.route';

export default () => {
	const app = Router();

	cities(app); 

	return app;
};
